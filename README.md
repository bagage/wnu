# Welcome New User (WNU)

This application is responsible for triggering notification to new user signing up to welcome him/her.

It runs in an AWS Lambda connected to the SNS topic for data.

The SNS is used as an event source for the Lambda function which processes new user. For now, we persist previous users in Lambda memory but also in a S3 bucket, to ensure that we have at least 3 other people available. This bucket it refreshed every 2 hours to avoid API rate limit (2000 requests per months). I find it acceptable for now since these cached users are used only if there are not enough new users to welcome at a time - but the strategy could be easily changed. Switching to Amazon Store Parameters could ease that by the way.

Ideally, we should instead add a SQS after the SNS, so that we don't need to use the S3 cache when batch is big enough. Say we have 5 users "Paul, Jack, Peter, Mark, Sandra", then the Lambda could submit the following 5 notifications:
* Hi Paul [...]. Jack, Peter and Mark also joined recently.
* Hi Jack [...]. Peter, Mark and Sandra also joined recently.
* Hi Peter [...]. Mark, Sandra and Paul also joined recently.
* Hi Mark [...]. Sandra, Paul and Jack also joined recently.
* Hi Sandra [...]. Paul, Jack and Peter also joined recently.

## Quick Start

1. Create an AWS user with the following roles:
    * AmazonS3FullAccess
    * AWSCloudFormationFullAccess 
    * AWSLambdaFullAccess 
    * CloudWatchLogsFullAccess
    * IAMFullAccess
2. Expose the following env vars (eg. `export WNU_APP_NAME=foo`):
    * WNU_APP_NAME: Application name to welcome user to
    * WNU_EMAIL_SENDER: Mail of notification creator
    * WNU_BACKEND_URL: Backend URL to notify for notification creation
    * WNU_SNS_ARN: ARN of the SNS event source
3. Configure `serverless`: `yarn install && serverless config credentials -p aws -k $key -s $secret`
4. Deploy application: `./gradlew deploy`
5. (optional) Once done, you can remove all resources with `./gradlew remove`

## Possible future improvements

### Error handling

Currently, there is no fallback in case of error (backend outage, AWS access error, temporary error..) - new users will never be welcome ever.

In production, we would probably prefer to really handle distinct errors properly, for instance by putting failed users in a SQS for later processing.

### Replace S3 caching solution

Instead of using a S3 bucket as caching solution, a [Parameter Store](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-parameter-store.html) could be a more adapted solution. 

### Add a SQS

Adding a queue would decrease number of lambda invocations and avoid fetching S3 bucket when batches are big enough.

### Ensure uniqueness

By default, Lambda might be invoked multiple times with the same user. Some unique validation strategy should be setup to avoid notifying twice the same user.
