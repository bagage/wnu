package net.damsy.wnu

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.lambda.runtime.events.SNSEvent
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.apache.logging.log4j.LogManager
import java.time.Duration
import java.time.LocalDateTime
import java.time.LocalTime

@Suppress("unused")
class Handler : RequestHandler<SNSEvent, String> {
  private val appName = System.getenv("WNU_APP_NAME")
  private val emailSender = System.getenv("WNU_EMAIL_SENDER")
  private val backendUrl = System.getenv("WNU_BACKEND_URL")
  private val bucket = System.getenv("WNU_BUCKET_NAME")

  private val mapper = JacksonDateMapper().mapper
  private val persistence = UserPersistenceS3(bucket)
  private val welcome = Welcome(appName, emailSender, backendUrl)

  override fun handleRequest(input: SNSEvent, context: Context): String {
    if (cached == null) {
      cached = persistence.getCachedUsers()
    } else {
      println("previous users loaded from Lambda cache")
    }
    val prevUsers = cached?.users ?: emptyList()
    val refreshCache = cached == null || Duration.between(LocalTime.now(), cached?.datetime).toHours() > 2

    val newUsers = input.records.map { it.sns.message }.map { mapper.readValue<User>(it) }.filter { !prevUsers.map { p -> p.id}.contains(it.id) }

    println("welcoming $newUsers, with older $prevUsers")

    welcome.buildNotification(newUsers, prevUsers).map { welcome.submit(it) }

    val nextUsers = prevUsers.plus(newUsers).takeLast(welcome.alsoJoinedLimit)
    cached = CachedUsers(cached?.datetime ?: LocalDateTime.now(), nextUsers) // refresh cached list
    if (refreshCache || prevUsers.size < welcome.alsoJoinedLimit) {
      println("refresh cache with ${nextUsers.map { it.name }}")
      persistence.setCachedUsers(nextUsers)
    }

    return "welcomed: $newUsers"
  }

  companion object {
    // if the lambda is not destroyed between 2 invocations, this object will already be loaded, avoiding to use S3
    var cached: CachedUsers? = null
  }
}
