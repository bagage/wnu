package net.damsy.wnu

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

class JacksonDateMapper {
  val mapper: ObjectMapper = jacksonObjectMapper().registerModule(JavaTimeModule())
}
