package net.damsy.wnu

data class Notification(val sender: String, val receiver: Number, val message: String, val recentUserIds: List<Number>)
