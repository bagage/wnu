package net.damsy.wnu

import com.fasterxml.jackson.annotation.JsonProperty

data class User(val name: String, val id: Number, @JsonProperty("created_at") val createdAt: String)
