package net.damsy.wnu

interface UserPersistenceInterface {
  fun getCachedUsers(): List<User>
  fun setCachedUsers(users: List<User>)
}
