package net.damsy.wnu

import java.time.LocalDateTime

data class CachedUsers(val datetime: LocalDateTime, val users: List<User>)
