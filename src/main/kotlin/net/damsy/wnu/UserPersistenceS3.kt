package net.damsy.wnu

import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.apache.logging.log4j.LogManager
import java.io.BufferedReader
import java.time.LocalDateTime

class UserPersistenceS3(private val bucket: String, private val bucketKey: String = "recent-users") {
  private val mapper = JacksonDateMapper().mapper

  fun getCachedUsers(): CachedUsers? {
    val s3 = AmazonS3ClientBuilder.defaultClient()
    if (!s3.doesObjectExist(bucket, bucketKey))
      return null

    val s3obj = s3.getObject(bucket, bucketKey)
    val stream = s3obj.objectContent.readBytes().inputStream()
    val content = stream.bufferedReader().use(BufferedReader::readText)

    println("Retrieved S3 content: $content")

    return mapper.readValue(content)
  }


  fun setCachedUsers(users: List<User>) {
    val cachedUsers = CachedUsers(LocalDateTime.now(), users)
    val s3 = AmazonS3ClientBuilder.defaultClient()
    println("Writing S3 content: $cachedUsers")
    val content = mapper.writeValueAsString(cachedUsers)
    s3.putObject(bucket, bucketKey, content)
  }
}
