package net.damsy.wnu

import org.apache.logging.log4j.LogManager

class Welcome(private val appName: String, private val emailSender: String, private val backendUrl: String) {
  val alsoJoinedLimit = 3

  fun buildNotification(newUsers: List<User>, previousUsers: List<User>): List<Notification> {
    return newUsers.mapIndexed { index, user ->
      val before = if (index > 0) newUsers.slice(IntRange(0, index - 1)) else emptyList()
      val after = if (index < newUsers.size - 1) newUsers.slice(IntRange(index + 1, newUsers.size - 1)) else emptyList()
      val otherUsers = before.plus(after).plus(previousUsers).take(this.alsoJoinedLimit)
      val othersFmt = otherUsers.joinToString { it.name }.reversed().replaceFirst(",", " and".reversed()).reversed()
      val message = "Hi ${user.name}, welcome to ${appName}. ${if (otherUsers.isEmpty()) "Enjoy your ride!" else "$othersFmt also joined recently."}"

      Notification(emailSender, user.id, message, otherUsers.map { it.id })
    }
  }

  fun submit(notification: Notification): Boolean {
    val response = khttp.post(
        url = backendUrl,
        json = mapOf(
            "sender" to notification.sender,
            "receiver" to notification.receiver,
            "message" to notification.message,
            "recent_user_ids" to notification.recentUserIds
        )
    )
    LOG.debug("Response was ${response.statusCode}: ${response.text}")

    return response.statusCode == 200;
  }

  companion object {
    private val LOG = LogManager.getLogger(Handler::class.java)
  }

}
