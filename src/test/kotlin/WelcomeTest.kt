import net.damsy.wnu.CachedUsers
import net.damsy.wnu.JacksonDateMapper
import net.damsy.wnu.Notification
import net.damsy.wnu.User
import net.damsy.wnu.Welcome
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

class WelcomeTest {
  private val appName = "foo"
  private val emailSender = "email@example.com"
  private val backendUrl = "some-backendUrl"

  private fun user(name: String) = User(name, (Math.random() * 1000).toInt(), "")

  @Test
  fun buildNotificationOneUserNoPrev() {
    val users = listOf(user("Marc"))
    val actual = Welcome(appName, emailSender, backendUrl).buildNotification(users, emptyList())
    val expected = listOf(Notification(emailSender, users[0].id, "Hi Marc, welcome to foo. Enjoy your ride!", emptyList()))

    assertEquals(expected, actual)
  }

  @Test
  fun buildNotificationOneUser4Prev() {
    val users = listOf(
        user("Celine")
    )
    val prev = listOf(
        user("Marc"),
        user("Joseph"),
        user("Louis"),
        user("Peter")
    )
    val actual = Welcome(appName, emailSender, backendUrl).buildNotification(users, prev)
    val expected = listOf(
        Notification(emailSender, users[0].id, "Hi Celine, welcome to foo. Marc, Joseph and Louis also joined recently.", listOf(prev[0], prev[1], prev[2]).map { it.id })
    )

    assertEquals(expected, actual)
  }

  @Test
  fun buildNotification4UsersNoPrev() {
    val users = listOf(
        user("Marc"),
        user("Joseph"),
        user("Louis"),
        user("Peter")
    )
    val actual = Welcome(appName, emailSender, backendUrl).buildNotification(users, emptyList())
    val expected = listOf(
        Notification(emailSender, users[0].id, "Hi Marc, welcome to foo. Joseph, Louis and Peter also joined recently.", listOf(users[1], users[2], users[3]).map { it.id }),
        Notification(emailSender, users[1].id, "Hi Joseph, welcome to foo. Marc, Louis and Peter also joined recently.", listOf(users[0], users[2], users[3]).map { it.id }),
        Notification(emailSender, users[2].id, "Hi Louis, welcome to foo. Marc, Joseph and Peter also joined recently.", listOf(users[0], users[1], users[3]).map { it.id }),
        Notification(emailSender, users[3].id, "Hi Peter, welcome to foo. Marc, Joseph and Louis also joined recently.", listOf(users[0], users[1], users[2]).map { it.id })
    )

    assertEquals(expected, actual)
  }

  @Test
  fun buildNotification4Users4Prev() {
    val users = listOf(
        user("Celine"),
        user("Sacha"),
        user("Marcus"),
        user("Josh")
    )
    val prev = listOf(
        user("Marc"),
        user("Joseph"),
        user("Louis"),
        user("Peter")
    )
    val actual = Welcome(appName, emailSender, backendUrl).buildNotification(users, prev)
    val expected = listOf(
        Notification(emailSender, users[0].id, "Hi Celine, welcome to foo. Sacha, Marcus and Josh also joined recently.", listOf(users[1], users[2], users[3]).map { it.id }),
        Notification(emailSender, users[1].id, "Hi Sacha, welcome to foo. Celine, Marcus and Josh also joined recently.", listOf(users[0], users[2], users[3]).map { it.id }),
        Notification(emailSender, users[2].id, "Hi Marcus, welcome to foo. Celine, Sacha and Josh also joined recently.", listOf(users[0], users[1], users[3]).map { it.id }),
        Notification(emailSender, users[3].id, "Hi Josh, welcome to foo. Celine, Sacha and Marcus also joined recently.", listOf(users[0], users[1], users[2]).map { it.id })
    )

    assertEquals(expected, actual)
  }
}
